# -*- coding: utf-8 -*-
from django.db import models


class SkillCategory(models.Model):
    name = models.CharField(max_length=100, verbose_name='Skill category name')

    def __str__(self):
        return '%s' % self.name

    def __unicode__(self):
        return '%s' % self.name
