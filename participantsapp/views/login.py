from django.http import HttpResponseRedirect
from django.views.generic.edit import FormView
from django.contrib.auth import login

from participantsapp.forms import EmailAuthenticationForm


class LoginFormView(FormView):
    form_class = EmailAuthenticationForm

    template_name = "login.html"

    success_url = "/quiz"

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect('/quiz')
        else:
            return super(LoginFormView, self).dispatch(
                request, *args, **kwargs)

    def form_valid(self, form):
        login(self.request, form.get_user())

        return super(LoginFormView, self).form_valid(form)
