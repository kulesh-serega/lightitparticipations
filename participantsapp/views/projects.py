from django.views.generic import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import models
from django.db.models import Value
from django.db.models.functions import Concat

from participantsapp.models import Project


class ProjectsView(LoginRequiredMixin, ListView):
    model = Project

    template_name = 'projects.html'

    context_object_name = "projects"

    paginate_by = 10

    def get_queryset(self):
        list_filters = []
        key_filters = {}

        if not self.request.user.is_staff:
            key_filters['developer_id'] = self.request.user.id

        if self.request.GET.get('search'):
            search = self.request.GET.get('search').strip()
            list_filters.append(
                models.Q(name__icontains=search) |
                models.Q(manager_name__icontains=search) |
                models.Q(url__icontains=search) |
                models.Q(full_name__icontains=search) |
                models.Q(skill__name__icontains=search)
            )

        order_by = self.request.GET.get('orderby') or 'name'
        full_name = Concat(
            'developer__last_name', Value(' '), 'developer__first_name')
        manager_name = Concat(
            'manager__last_name', Value(' '), 'manager__first_name')

        return Project.objects.annotate(
            full_name=full_name, manager_name=manager_name).distinct().filter(
                *list_filters, **key_filters).order_by(order_by)
