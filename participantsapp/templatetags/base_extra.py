import urllib

from django import template

register = template.Library()


@register.filter
def glyphicon_tags(value):
    """
    Get icons by user fields name
    """
    tags = [
        ('first_name', 'user'),
        ('last_name', 'user'),
        ('email', 'envelope'),
        ('work_from_date', 'calendar'),
    ]

    if 'password' in value:
        return 'lock'

    for search, replace in tags:
        value = value.replace(search, replace)

    return value


@register.filter
def get_last_url_part(url):
    return url.split('/')[-1]


@register.simple_tag
def add_url_params(get_params, order_by):
    params = {}

    reverse_order = '-' + order_by

    for key, value in get_params.iteritems():
        if key == 'orderby' and value == order_by:
            params[key] = reverse_order
        elif key == 'orderby' and value == reverse_order:
            params[key] = order_by

        if key != 'orderby':
            params[key] = value.encode('utf-8')

    if 'orderby' not in get_params or \
            (get_params['orderby'] != order_by and
             get_params['orderby'] != reverse_order):
        params['orderby'] = order_by

    return urllib.urlencode(params)


@register.simple_tag
def is_order_by(get_params, order_by):
    reverse_order = '-' + order_by

    if 'orderby' in get_params and get_params['orderby'] == order_by:
        return 'glyphicon-triangle-bottom'
    elif 'orderby' in get_params and get_params['orderby'] == reverse_order:
        return 'glyphicon-triangle-top'
