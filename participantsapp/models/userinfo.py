# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User


class UserInfo(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    work_from_date = models.DateTimeField(verbose_name='Work from date')

    def __str__(self):
        return '%s' % self.user

    def __unicode__(self):
        return '%s' % self.user
