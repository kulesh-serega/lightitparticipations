from django.views.generic.edit import FormView
from django.contrib.auth.mixins import LoginRequiredMixin

from participantsapp.forms import ProjectForm


class AddProjectFormView(LoginRequiredMixin, FormView):
    form_class = ProjectForm

    template_name = "quiz.html"

    success_url = "/projects"

    def form_valid(self, form):
        form.save()
        return super(AddProjectFormView, self).form_valid(form)
