# -*- coding: utf-8 -*-
from django.db import models


class Skill(models.Model):
    name = models.CharField(max_length=100, verbose_name='Skill name')
    category = models.ForeignKey(
        'SkillCategory', verbose_name='Skill category instance')

    def __str__(self):
        return '%s' % self.name

    def __unicode__(self):
        return '%s' % self.name
