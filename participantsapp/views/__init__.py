from participantsapp.views.register import RegisterFormView
from participantsapp.views.login import LoginFormView
from participantsapp.views.logout import LogoutView
from participantsapp.views.index import IndexView
from participantsapp.views.projects import ProjectsView
from participantsapp.views.addproject import AddProjectFormView
from participantsapp.views.projectdetails import ProjectDetailView, get_file
