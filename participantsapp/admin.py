from django.contrib import admin

from participantsapp.models import UserInfo, Project, Skill, \
    SkillCategory, Manager
from participantsapp.forms import EmailAuthenticationForm


admin.site.register(UserInfo)
admin.site.register(Manager)
admin.site.register(Project)
admin.site.register(Skill)
admin.site.register(SkillCategory)
admin.site.login_form = EmailAuthenticationForm
admin.site.login_template = 'admin_login.html'
