from django.test import TestCase
from participantsapp.forms import RegisterForm, EmailAuthenticationForm, \
    ProjectForm


class ParticipantsAppFormsTestCase(TestCase):
    fixtures = [
        'tests/projects.json', 'tests/users.json', 'manager.json',
        'skillcategory.json', 'skill.json']

    def test_registration_form(self):
        form_data = {
            'first_name': 'First',
            'last_name': 'Last',
            'email': 'first@mail.ru',
            'work_from_date': '2016-11-01',
            'password1': 'qwe',
            'password2': 'qwe'
        }
        form = RegisterForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_authentication_form(self):
        form_data = {
            'email': 'admin@mail.ru',
            'password': '@0dm1n**'
        }
        form = EmailAuthenticationForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_project_form(self):
        form_data = {
            'name': 'Petidco',
            'url': 'http://petidco.com',
            'reason': 'url exist',
            'manager': 2,
            'date_start': '2016-11-01',
            'date_end': '2016-11-19',
            'live': True,
            'description': 'Project description',
            'skill': ['34', '141'],
            'developer': 25
        }
        form = ProjectForm(data=form_data)
        self.assertTrue(form.is_valid())
