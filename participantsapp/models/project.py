# -*- coding: utf-8 -*-
from django.db import models
from django.conf import settings


class Project(models.Model):
    name = models.CharField(max_length=100, verbose_name='Project name')
    live = models.BooleanField(
        default=False, verbose_name='Is this project live')
    url = models.CharField(
        blank=True, max_length=255, verbose_name='Project URL')
    url_does_not_exist = models.BooleanField(
        default=False, verbose_name='There is no URL for this project')
    reason = models.CharField(
        blank=True, max_length=255,
        verbose_name='Why there is no URL for this project?')
    manager = models.ForeignKey(
        'Manager', verbose_name='Project manager instance')
    date_start = models.DateTimeField(verbose_name='Start date')
    date_end = models.DateTimeField(
        null=True, blank=True, verbose_name='End date')
    working_now = models.BooleanField(
        default=False, verbose_name='Still working on this project')
    developer = models.ForeignKey(settings.AUTH_USER_MODEL)
    description = models.TextField(verbose_name='Project description')
    skill = models.ManyToManyField('Skill', verbose_name='Skill list')
    nda = models.BooleanField(
        default=False, verbose_name='Non-disclosure agreement')
    files = models.ManyToManyField(
        'ProjectAttachFile', blank=True, verbose_name='Files list')

    def __str__(self):
        return '%s' % self.name

    def __unicode__(self):
        return '%s' % self.name
