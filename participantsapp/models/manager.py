# -*- coding: utf-8 -*-
from django.db import models


class Manager(models.Model):
    first_name = models.CharField(max_length=100, verbose_name='First name')
    last_name = models.CharField(max_length=100, verbose_name='Last name')

    def __str__(self):
        return '%s %s' % (self.last_name, self.first_name)

    def __unicode__(self):
        return '%s %s' % (self.last_name, self.first_name)
