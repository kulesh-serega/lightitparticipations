# coding: utf-8
import mimetypes

from django.views.generic import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from participantsapp.models import Project, ProjectAttachFile


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project

    template_name = 'project-details.html'

    context_object_name = 'proj_details'

    def get_queryset(self):
        return Project.objects.all()


def get_file(request, pk):
    f = get_object_or_404(ProjectAttachFile, pk=pk)

    fsock = open(f.file.name, "rb")

    mimetypes.init()
    fname = f.file.name.split('/')[-1]
    mime_type_guess = mimetypes.guess_type(fname)

    response = HttpResponse(fsock, content_type=mime_type_guess[0])
    response['Content-Disposition'] = \
        'attachment; filename="{}"'.format(fname)
    response['Content-Length'] = f.file.size

    return response
