from django.conf.urls import url
from participantsapp import views


urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^register/$', views.RegisterFormView.as_view(), name='register'),
    url(r'^login/$', views.LoginFormView.as_view(), name='login'),
    url(r'^logout/$', views.LogoutView.as_view(), name='logout'),
    url(r'^quiz/$', views.AddProjectFormView.as_view(), name='quiz'),
    url(r'^projects/$', views.ProjectsView.as_view(), name='projects'),
    url(r'^project-details/(?P<pk>[0-9]+)/$',
        views.ProjectDetailView.as_view(),
        name='project_details'),
    url(r'file/(?P<pk>\d+)/$', views.get_file, name='get_project_file')
]
