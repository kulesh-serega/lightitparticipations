from django.http import HttpResponseRedirect
from django.views.generic.base import View


class IndexView(View):
    def get(self, request):
        return HttpResponseRedirect("/login")
