from django.utils.safestring import mark_safe
from django import forms


class HorizRadioRenderer(forms.RadioSelect.renderer):
    """
    this overrides widget method to put radio buttons horizontally
    instead of vertically.
    """
    def render(self):
        """Outputs radios"""
        return mark_safe(u'\n'.join([u'%s\n' % w for w in self]))


class HorizRadioSelect(forms.RadioSelect):
    renderer = HorizRadioRenderer
