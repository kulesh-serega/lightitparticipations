from django.test import TestCase
from django.contrib.auth.models import User

from participantsapp.models import Manager, Project, Skill, SkillCategory


class ParticipantsAppModelsTestCase(TestCase):
    """
    test creation of models and check their representation
    """
    def test_project_add_entry_and_models_string_representation(self):
        user = User.objects.create_user('test', 'test@gmail.com', 'test')
        self.assertTrue(user)
        self.assertEqual(str(user), user.username)

        manager = Manager.objects.create(
            last_name='Paleshko', first_name='Aleksandr')
        self.assertTrue(manager)
        name = '%s %s' % (manager.last_name, manager.first_name)
        self.assertEqual(str(manager), name)

        project = Project.objects.create(
            name='Petidco', live=True, url='http://petidco.com',
            reason='url exist', manager=manager, developer=user,
            date_start='2014-04-01', date_end='2014-11-15',
            description='Project description')
        self.assertTrue(project)
        self.assertEqual(str(project), project.name)

    def test_skill_add_entry_and_models_string_representation(self):
        skillcategory = SkillCategory(name='Languages')
        self.assertTrue(skillcategory)
        self.assertEqual(str(skillcategory), skillcategory.name)

        skill = Skill(name='PHP', category=skillcategory)
        self.assertTrue(skill)
        self.assertEqual(str(skill), skill.name)
