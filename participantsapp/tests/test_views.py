# -*- coding: utf-8 -*-
from django.test import TestCase


class ParticipantsAppViewsTestCase(TestCase):
    fixtures = [
        'tests/projects.json', 'tests/users.json', 'manager.json',
        'skillcategory.json', 'skill.json']

    def test_index(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 302)

    def test_register(self):
        resp = self.client.get('/register/')
        self.assertEqual(resp.status_code, 200)

    def test_login(self):
        response = self.client.get('/login/')
        self.assertEquals(response.status_code, 200)
        self.assertTrue('Login' in response.content)
        logged_in = self.client.login(
            email='admin@mail.ru', password='@0dm1n**')
        self.assertTrue(logged_in)

    def test_quiz(self):
        self.client.login(email='admin@mail.ru', password='@0dm1n**')
        resp = self.client.get('/quiz/')
        self.assertEqual(resp.status_code, 200)

    def test_projectdetails(self):
        self.client.login(email='admin@mail.ru', password='@0dm1n**')
        resp = self.client.get('/project-details/20/')
        self.assertEqual(resp.status_code, 200)

    def test_projects(self):
        self.client.login(email='admin@mail.ru', password='@0dm1n**')
        resp = self.client.get('/projects/')
        self.assertEqual(resp.status_code, 200)

    def test_logout(self):
        self.client.login(email='admin@mail.ru', password='@0dm1n**')
        response = self.client.get('/quiz/')
        self.assertEquals(response.status_code, 200)
        self.assertTrue('Log out' in response.content)
        self.client.logout()
        response = self.client.get('/quiz/')
        self.assertEquals(response.status_code, 302)
