import psycopg2
from xlrd import open_workbook

conn = psycopg2.connect("host=%s port=%s dbname=%s user=%s password=%s" % (
        '127.0.0.1',
        '5432',
        'pp_db',
        'pp_user',
        'pppswrd',))


def copy_data_to_db():
    wb = open_workbook("skills.xlsx")

    sheet = wb.sheet_by_index(0)

    sheet_list = []
    num_rows = sheet.nrows - 1
    curr_row = -1
    while curr_row < num_rows:
        curr_row += 1
        sheet.cell_value(curr_row, 0)
        sheet_list.append((
            (sheet.cell_value(curr_row, 0)),
            int(sheet.cell_value(curr_row, 1))))

    print sheet_list

    cur = conn.cursor()
    cur.executemany(
        """INSERT INTO participantsapp_skill(name, category_id)
        VALUES (%s, %s)""",
        sheet_list)
    conn.commit()
    conn.close()


if __name__ == '__main__':
    copy_data_to_db()
