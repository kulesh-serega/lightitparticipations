# -*- coding: utf-8 -*-
from django.db import models


class ProjectAttachFile(models.Model):
    file = models.FileField(upload_to='files', verbose_name='File name')

    def __str__(self):
        return '{}'.format(self.id)

    def __unicode__(self):
        return '{}'.format(self.id)
