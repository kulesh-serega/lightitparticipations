from django import forms
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError

from multiupload.fields import MultiFileField

from participantsapp.models import Project, ProjectAttachFile
from lightitparticipants.widgets import HorizRadioSelect


class ProjectForm(forms.ModelForm):
    live = forms.ChoiceField(
        widget=HorizRadioSelect,
        choices=[(True, 'Yes'), (False, 'No')],
        initial=True)
    files = MultiFileField(
        max_num=10, max_file_size=1024*1024*5)

    use_required_attribute = False

    class Meta:
        model = Project

        fields = (
            'name', 'url', 'url_does_not_exist', 'reason', 'manager',
            'date_start', 'date_end', 'working_now', 'live',
            'description', 'skill', 'developer', 'files')

    def __init__(self, *args, **kwargs):
        super(ProjectForm, self).__init__(*args, **kwargs)

        self.fields['files'].required = False

        for field in self.fields.itervalues():
            field.widget.attrs.update({
                'class': 'form-control',
                'autocomplete': 'off'
            })

        self.fields['url'].widget.attrs.update({
            'value': 'http://'
        })

        self.fields['live'].widget.attrs.update({
            'class': '',
        })

    def clean(self):
        super(ProjectForm, self).clean()

        cleaned_data = self.cleaned_data

        if cleaned_data['url_does_not_exist'] and not cleaned_data['reason']:
            self._errors['reason'] = self.error_class(
                ['This field is required.'])

        if not cleaned_data['url_does_not_exist'] and not cleaned_data['url']:
            self._errors['url'] = self.error_class(
                ['This field is required.'])

        if not cleaned_data['url_does_not_exist'] and cleaned_data['url']:
            validate = URLValidator()
            url = self.cleaned_data.get('url')

            try:
                validate(url)
            except ValidationError:
                self._errors['url'] = self.error_class(
                    ['URL is not valid.'])

        return cleaned_data

    def save(self):
        projectform = super(ProjectForm, self).save(commit=False)
        projectform.save()

        for skill in self.cleaned_data['skill']:
            skill.save()
            projectform.skill.add(skill)

        for file in self.cleaned_data['files']:
            attachment = ProjectAttachFile(file=file)
            attachment.save()
            projectform.files.add(attachment)

        projectform.save()

        return projectform
